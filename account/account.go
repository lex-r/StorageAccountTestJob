package account

// ID represents unique identifier for accounts
type ID uint

// Account represents the model of an account
type Account struct {
	ID      ID      `json:"ID"`
	Balance float64 `json:"balance"`
}
