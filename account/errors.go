package account

import "fmt"

// ErrAccountNotFound returned when the account not found
type ErrAccountNotFound struct {
	ID ID
}

// NewErrAccountNotFound returns new ErrAccountNotFound
func NewErrAccountNotFound(id ID) ErrAccountNotFound {
	return ErrAccountNotFound{id}
}

// Error returns the string representation of ErrAccountNotFound
func (e ErrAccountNotFound) Error() string {
	return fmt.Sprintf("account `%d` not found", e.ID)
}

// ErrInsufficientFunds returned when there is not enough funds in the account balance
type ErrInsufficientFunds struct {
	ID ID
}

// NewErrInsufficientFunds returns new ErrInsufficientFunds
func NewErrInsufficientFunds(id ID) ErrInsufficientFunds {
	return ErrInsufficientFunds{id}
}

// ErrInsufficientFunds returns the string representation of ErrInsufficientFunds
func (e ErrInsufficientFunds) Error() string {
	return fmt.Sprintf("insufficient funds on account `%d`", e.ID)
}
