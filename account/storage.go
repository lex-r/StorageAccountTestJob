package account

import "sync"

// Storage provides methods for working with account: creating, retrieving and others.
type Storage struct {
	mu            sync.RWMutex
	autoincrement ID
	accounts      map[ID]Account
}

// NewStorage initializes and returns new instance of the storage
func NewStorage() *Storage {
	return &Storage{
		accounts: make(map[ID]Account),
	}
}

// CreateAccount creates new account and stores it in the storage
func (s *Storage) CreateAccount(balance float64) Account {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.autoincrement++
	newAcc := Account{
		ID:      s.autoincrement,
		Balance: balance,
	}
	s.accounts[newAcc.ID] = newAcc

	return newAcc
}

// GetAccount returns an account by ID if it exists in the storage.
func (s *Storage) GetAccount(id ID) (*Account, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()

	acc, exists := s.accounts[id]
	if !exists {
		return nil, NewErrAccountNotFound(id)
	}

	return &acc, nil
}

// Transfer transfers funds from `from` account to `to` account
// if there is enough funds on `from` account.
func (s *Storage) Transfer(from ID, to ID, amount float64) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	fromAccount, exists := s.accounts[from]
	if !exists {
		return NewErrAccountNotFound(from)
	}

	toAccount, exists := s.accounts[to]
	if !exists {
		return NewErrAccountNotFound(to)
	}

	if fromAccount.Balance < amount {
		return NewErrInsufficientFunds(from)
	}

	fromAccount.Balance -= amount
	toAccount.Balance += amount
	s.accounts[from] = fromAccount
	s.accounts[to] = toAccount

	return nil
}
