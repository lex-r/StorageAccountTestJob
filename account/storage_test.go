package account

import "testing"

func Test_CreateAccount(t *testing.T) {
	s := NewStorage()

	acc := s.CreateAccount(42)
	if acc.Balance != 42 {
		t.Error("wrong balance on new account")
	}
}

func Test_CreateAccount_IncrementsIdentifier(t *testing.T) {
	s := NewStorage()

	for i := 1; i < 10; i++ {
		acc1 := s.CreateAccount(100)
		if acc1.ID != ID(i) {
			t.Errorf("wrong ID on new account, expected value: 1, actual value: %d", i)
		}
	}
}

func Test_GetAccount_ExistingAccount(t *testing.T) {
	s := NewStorage()

	acc := s.CreateAccount(100)

	actualAcc, err := s.GetAccount(acc.ID)
	if err != nil {
		t.Error("error retrieving account")
	}

	if actualAcc.Balance != acc.Balance {
		t.Error("wrong balance on retrieved account")
	}

	if actualAcc.ID != acc.ID {
		t.Error("wrong ID on retrieved account")
	}
}

func Test_GetAccount_NonExistentAccount(t *testing.T) {
	s := NewStorage()

	acc, err := s.GetAccount(100)
	if acc != nil && err == nil {
		t.Error("retrieving nonexistent account must return an error")
	}
}

func Test_Transfer_Successful(t *testing.T) {
	s := NewStorage()

	fromAcc := s.CreateAccount(100)
	toAcc := s.CreateAccount(200)

	err := s.Transfer(fromAcc.ID, toAcc.ID, 50)
	if err != nil {
		t.Error("error transferring funds")
	}

	newFromAcc, _ := s.GetAccount(fromAcc.ID)
	newToAcc, _ := s.GetAccount(toAcc.ID)

	if newFromAcc.Balance != 50 {
		t.Error("wrong balance after transferring funds")
	}

	if newToAcc.Balance != 250 {
		t.Error("wrong balance after transferring funds")
	}
}

func Test_Transfer_NotEnoughFunds(t *testing.T) {
	s := NewStorage()

	fromAcc := s.CreateAccount(100)
	toAcc := s.CreateAccount(200)

	err := s.Transfer(fromAcc.ID, toAcc.ID, 150)
	if err == nil {
		t.Error("error transferring must return error when there is not enough funds on the account balance")
	}

	newFromAcc, _ := s.GetAccount(fromAcc.ID)
	newToAcc, _ := s.GetAccount(toAcc.ID)

	if newFromAcc.Balance != 100 {
		t.Error("wrong balance after transferring funds")
	}

	if newToAcc.Balance != 200 {
		t.Error("wrong balance after transferring funds")
	}
}

//TODO: add other tests for the Transfer method for checking other cases

//TODO: add concurrency tests
