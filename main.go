package main

import (
	"flag"
	"log"
	"net/http"

	"gitlab.com/lex-r/StorageAccountTestJob/account"
)

var (
	addr = flag.String("addr", ":8080", "addr for listening")
)

func main() {

	flag.Parse()

	storage := account.NewStorage()

	http.HandleFunc("/account", AccountMethod(storage))
	http.HandleFunc("/transfer", TransferMethod(storage))

	log.Printf("start listening on %s", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		panic(err)
	}
}
