package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"gitlab.com/lex-r/StorageAccountTestJob/account"
)

// AccountMethod returns http handler function for working with accounts
func AccountMethod(storage *account.Storage) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		if req.Method == http.MethodGet {

			// get an account

			queryId := req.URL.Query().Get("id")
			id, err := strconv.ParseUint(queryId, 10, 64)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				fmt.Fprint(w, "the value of id must be uint64")
				return
			}

			acc, err := storage.GetAccount(account.ID(id))
			if err != nil {
				switch err.(type) {
				case account.ErrAccountNotFound:
					w.WriteHeader(http.StatusNotFound)
					fmt.Fprint(w, err.Error())
					return
				default:
					log.Printf("Error retrieving account with id(%d): %s", id, err)
					w.WriteHeader(http.StatusInternalServerError)
					return
				}
			}

			jsonAcc, _ := json.Marshal(acc)
			fmt.Fprint(w, string(jsonAcc))

		} else if req.Method == http.MethodPost {

			// create an account

			queryBalance := req.URL.Query().Get("balance")
			balance, err := strconv.ParseFloat(queryBalance, 10)
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				fmt.Fprint(w, "the value of balance must be float64")
				return
			}

			newAcc := storage.CreateAccount(balance)
			jsonAcc, _ := json.Marshal(newAcc)
			fmt.Fprint(w, string(jsonAcc))
		}
	}
}

// TransferMethod returns http handler function for transferring funds between accounts
func TransferMethod(storage *account.Storage) func(w http.ResponseWriter, req *http.Request) {
	return func(w http.ResponseWriter, req *http.Request) {
		queryFrom := req.URL.Query().Get("from")
		from, err := strconv.ParseUint(queryFrom, 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "the value of the from parameter must be uint64")
			return
		}

		queryTo := req.URL.Query().Get("to")
		to, err := strconv.ParseUint(queryTo, 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "the value of the to parameter must be uint64")
			return
		}

		queryAmount := req.URL.Query().Get("amount")
		amount, err := strconv.ParseFloat(queryAmount, 10)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			fmt.Fprint(w, "the value of the amount parameter must be float64")
			return
		}

		err = storage.Transfer(account.ID(from), account.ID(to), amount)
		if err != nil {
			switch err.(type) {
			case account.ErrAccountNotFound:
				errAccountNotFound := err.(account.ErrAccountNotFound)
				w.WriteHeader(http.StatusNotFound)
				fmt.Fprintf(w, "account with id=%d not found", errAccountNotFound.ID)
				return
			case account.ErrInsufficientFunds:
				errInsufficientFunds := err.(account.ErrInsufficientFunds)
				w.WriteHeader(http.StatusBadRequest)
				fmt.Fprintf(w, "not enough funds on the acccount with id=%d", errInsufficientFunds.ID)
				return
			default:
				w.WriteHeader(http.StatusInternalServerError)
				log.Printf("Error transferring funds between account %d and %d: %s", from, to, err)
				return
			}
		}

		fmt.Fprint(w, http.StatusOK)
	}
}
